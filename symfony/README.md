# Docker Compose
## Symfony
### Requirements
- MariaDB 10: an available MariaDB server running on port `3306` and host `db` with the username as `symfony`, the password as `symfony` and a database named `symfony`.

### How to run
Copy the environment file
```sh
cp .env .env.local
```

Install dependencies with composer
```sh
composer install
```

Create the database if it does not exist
```sh
php bin/console doctrine:database:create --if-not-exists
```

Create the database schema
```sh
php bin/console doctrine:schema:update
```

### Command to do too initialize docker :
First we build the dockerfile :
```
docker-compose build
```
Next we create the app container :
```
docker-compose up -d
```
And finally we install project dependencies :
```
docker-compose exec app composer install
```
And to finish we go here to check that everything works: [http://localhost:8080](http://localhost:8080)

### Explanation : why dit I put a .gitignore in storage ?
Just in order to take in account the directery storage in my git push